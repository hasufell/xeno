{-# LANGUAGE CPP #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE StandaloneDeriving, DeriveGeneric #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

-- | Benchmark speed.

module Main where

import           Control.DeepSeq
import           Criterion
import           Criterion.Main
import           Data.ByteString (ByteString)
import qualified Data.ByteString as S
import qualified Data.ByteString.Lazy as L
import           GHC.Generics
import           System.FilePath.Posix
import qualified Text.XML.Expat.SAX as Hexpat
import qualified Text.XML.Expat.Tree as HexpatTree
import qualified Text.XML.Hexml as Hexml
import           Text.XML.Light as XML
import qualified Xeno.SAX
import qualified Xeno.Types
import qualified Xeno.DOM
import qualified Xeno.DOM.Robust
import           Xeno.Unicode
#ifdef LIBXML2
import qualified Text.XML.LibXML.Parser as Libxml2
#endif


benchFiles :: [(String, String)]
benchFiles = [ ("4KB",   "books-4kb.xml")
             , ("31KB",  "text-31kb.xml")
             , ("211KB", "fabricated-211kb.xml")
             ]


readFileZ :: FilePath -> IO (ByteString, Xeno.Types.ByteStringZeroTerminated)
readFileZ fn = do
    !s <- S.readFile fn
    let !sz = Xeno.Types.BSZT (s `S.snoc` 0)
    return (s,  sz)


readFileZUTF16 :: FilePath -> FilePath -> IO (ByteStringUTF16LE, ByteStringZeroTerminatedUTF16LE, ByteStringUTF16BE, ByteStringZeroTerminatedUTF16BE)
readFileZUTF16 lefn befn = do
    les <- S.readFile lefn
    let (!le, !lez) = case interpretAsUTF16 les of
            LittleEndian le'@(BSUTF16LE !bs) -> (le', BSZTUTF16LE $ bs `S.snoc` 0)
            _ -> error (lefn ++ ": wrong LE encoding")
    bes <- S.readFile befn
    let (!be, !bez) = case interpretAsUTF16 bes of
            BigEndian be'@(BSUTF16BE !bs) -> (be', BSZTUTF16BE $ bs `S.snoc` 0)
            _ -> error (befn ++ ": wrong BE encoding")
    return (le, lez, be, bez)


main :: IO ()
main = defaultMain $
    ((flip map) benchFiles
    $ \(group, fn) ->
        env (readFileZ ("data" </> fn))
            (\ ~(!input, !inputz) -> bgroup group
                 [ bench "hexml-dom" (whnf Hexml.parse input)
                 , bench "xeno-sax" (whnf Xeno.SAX.validate input)
                 , bench "xeno-sax-z" (whnf Xeno.SAX.validate inputz)
                 , bench "xeno-sax-ex" (whnf Xeno.SAX.validateEx input)
                 , bench "xeno-sax-ex-z" (whnf Xeno.SAX.validateEx inputz)
                 , bench "xeno-dom" (whnf Xeno.DOM.parse input)
                 , bench "xeno-dom-with-recovery" (whnf Xeno.DOM.Robust.parse input)
                 , bench
                     "hexpat-sax"
                     (whnf
                        ((Hexpat.parseThrowing Hexpat.defaultParseOptions :: L.ByteString -> [Hexpat.SAXEvent ByteString ByteString]) .
                         L.fromStrict)
                        input)
                 , bench
                     "hexpat-dom"
                     (whnf
                        ((HexpatTree.parse' HexpatTree.defaultParseOptions :: ByteString -> Either HexpatTree.XMLParseError (HexpatTree.Node ByteString ByteString)))
                        input)
                 , bench "xml-dom" (nf XML.parseXMLDoc input)
#ifdef LIBXML2
                 , bench "libxml2-dom" (whnfIO (Libxml2.parseMemory input))
#endif
                 ]))
    ++ ((flip map) benchFiles
    $ \(group, fn) ->
        env (readFileZUTF16 ("data" </> "utf16" </> "le" </> fn) ("data" </> "utf16" </> "be" </> fn))
            (\ ~(!inputle, !inputzle, !inputbe, !inputzbe) -> bgroup group
                 [ bench "xeno-sax-utf16-le"      (whnf Xeno.SAX.validate inputle)
                 , bench "xeno-sax-utf16-le-z"    (whnf Xeno.SAX.validate inputzle)
                 , bench "xeno-sax-utf16-be"      (whnf Xeno.SAX.validate inputbe)
                 , bench "xeno-sax-utf16-be-z"    (whnf Xeno.SAX.validate inputzbe)
                 --
                 , bench "xeno-sax-utf16-le-ex"   (whnf Xeno.SAX.validateEx inputle)
                 , bench "xeno-sax-utf16-le-z-ex" (whnf Xeno.SAX.validateEx inputzle)
                 , bench "xeno-sax-utf16-be-ex"   (whnf Xeno.SAX.validateEx inputbe)
                 , bench "xeno-sax-utf16-be-z-ex" (whnf Xeno.SAX.validateEx inputzbe)
                 ]))


deriving instance Generic Content
deriving instance Generic Element
deriving instance Generic CData
deriving instance Generic CDataKind
deriving instance Generic QName
deriving instance Generic Attr
instance NFData Content
instance NFData Element
instance NFData CData
instance NFData CDataKind
instance NFData QName
instance NFData Attr
